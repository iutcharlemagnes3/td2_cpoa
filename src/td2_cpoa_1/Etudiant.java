package td2_cpoa_1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Classe modélisant un étudiant
 * @author Capucine Blanchard, Maxime Gaudare
 */
public class Etudiant {

	Identite identitite;
	Formation formation;
	HashMap<String, ArrayList<Double>> resultat;

	/**
	 * Construit un étudiant à partir d'une formation et d'une identité
	 * @param pFormation, la formation de l'étudiant
	 * @param pIdentite, l'identité de l'étudiant
	 */
	public Etudiant(Formation pFormation, Identite pIdentite){
		this.formation = pFormation;
		this.identitite = pIdentite;
		resultat = new HashMap<String, ArrayList<Double>>();
		for (String nomMatiere : formation.getEnsembleMatiere().keySet()){
			resultat.put(nomMatiere, new ArrayList<Double>());
		}
	}

	public Identite getIdentitite() {
		return identitite;
	}

	public Formation getFormation() {
		return formation;
	}

	public HashMap<String, ArrayList<Double>> getResultat() {
		return resultat;
	}

	/**
	 * Ajoute une note dans les resultat de l'etudiant
	 * @param pNote, la note à ajouter
	 * @param pFormation, la formation concercé
	 * @throws IntervalleExcpetion
	 * @throws MatiereExcpetion
	 */
	public void ajouterNote (double pNote, String pMatiere) throws IntervalleExcpetion, MatiereNonExistanteException{

		ArrayList<Double> note = new ArrayList<>();

		if (!resultat.containsKey(pMatiere))
			throw new MatiereNonExistanteException();
		else if (pNote < 0 || pNote > 20)
			throw new IntervalleExcpetion();
		else if (resultat.get(pMatiere).isEmpty())
			note.add(pNote);
		else{
			note = resultat.get(pMatiere);
			note.add(pNote);
		}
		resultat.put(pMatiere, note);
	}

	public double calculerMoyenneMatiere (String pMatiere) throws MatiereNonExistanteException{
		if (!resultat.containsKey(pMatiere))
			throw new MatiereNonExistanteException();

		ArrayList<Double> liste = resultat.get(pMatiere);
		double moyenne = 0;

		for (Double note: liste)
			moyenne += note;

		return moyenne / liste.size();
	}
	
	public double calculerMoyenneGeneral () throws MatiereNonExistanteException{
		Double res = 0.0;
		Double coef = 0.0;
		for(String nomMat : formation.getEnsembleMatiere().keySet()){
			for (Iterator<Double> iterator = resultat.get(nomMat).iterator(); iterator.hasNext();) {
				res += iterator.next() * formation.donnerCoefficient(nomMat);
				coef += formation.donnerCoefficient(nomMat);
			}
		}
		return res/coef;
	}

}
