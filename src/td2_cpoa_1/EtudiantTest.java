package td2_cpoa_1;
import static org.junit.Assert.*;

import org.junit.Test;


public class EtudiantTest {

	@Test 
	public void testNotesOk() throws IntervalleExcpetion, MatiereNonExistanteException, MatiereDejaExistanteException {
		//creer une formation
		Formation formation = new Formation("formation");
		//ajoute une matiere a la formation
		formation.ajouterMatiere("matiere", 0.5);
		//creer une identite
		Identite identite = new Identite("id", "nom", "prenom");
		//creer un etudiant avec en parametre la formation et l'identite cree au dessus
		Etudiant etudiant = new Etudiant(formation, identite);
		//ajoute la note 10 a la matiere ajoutee precedemment a la formation
		etudiant.ajouterNote(10, "matiere");
		//variable contenant la premiere  note de l'etudiant
		double a =  etudiant.getResultat().get("matiere").get(0);
		//test la note de l'etudiant
	}
	
	@Test (expected=IntervalleExcpetion.class ) 
	public void testNotesNegative() throws IntervalleExcpetion, MatiereNonExistanteException, MatiereDejaExistanteException {
		//creer une formation
		Formation formation = new Formation("formation");
		//ajoute une matiere a la formation
		formation.ajouterMatiere("matiere", 0.5);
		//creer une identite
		Identite identite = new Identite("id", "nom", "prenom");
		//creer un etudiant avec en parametre la formation et l'identite cree au dessus
		Etudiant etudiant = new Etudiant(formation, identite);
		//ajoute la note 10 a la matiere ajoutee precedemment a la formation
		etudiant.ajouterNote(10, "matiere");
		double a =  etudiant.getResultat().get("matiere").get(0);
		assertEquals("le resultat devrait etre 10",a, 10.0, 0.01);
		//ajoute une note negative 
		a =  etudiant.getResultat().get("matiere").get(0);
		assertEquals("le resultat devrait etre 10",  a, 10.0 ,0.01);
		etudiant.ajouterNote(-5, "matiere");

	}

	@Test (expected=MatiereNonExistanteException.class ) 
	public void testMatiereNonExistante() throws IntervalleExcpetion, MatiereNonExistanteException, MatiereDejaExistanteException {
		//creer une formation
		Formation formation = new Formation("formation");
		//ajoute une matiere a la formation
		formation.ajouterMatiere("matiere", 0.5);
		//creer une identite
		Identite identite = new Identite("id", "nom", "prenom");
		//creer un etudiant avec en parametre la formation et l'identite cree au dessus
		Etudiant etudiant = new Etudiant(formation, identite);
		//ajoute la note 10 a la matiere ajoutee precedemment a la formation
		etudiant.ajouterNote(10, "matiere");
		double a =  etudiant.getResultat().get("matiere").get(0);
		etudiant.ajouterNote(5, "matiere");
		assertEquals("la moyenne devrait etre 7.5", 7.5 ,etudiant.calculerMoyenneMatiere("matiere"), 0.01);
		assertEquals("la moyenne generale devrait etre 7.5", 7.5, etudiant.calculerMoyenneGeneral(), 0.01);
		etudiant.ajouterNote(8, "nonExistant");
	}

}