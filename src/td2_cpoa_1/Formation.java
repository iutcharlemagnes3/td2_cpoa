package td2_cpoa_1;
import java.util.HashMap;


public class Formation {
	private String formation;
	private HashMap<String, Double> ensembleMatiere;

	public Formation(String f){
		this.formation=f;
		this.ensembleMatiere=new HashMap<String, Double>();
	}

	public String getFormation() {
		return formation;
	}

	public HashMap<String, Double> getEnsembleMatiere() {
		return ensembleMatiere;
	}
	/**
	 * methode ajoutant une matiere a la formation
	 * @param m matiere a ajouter
	 * @param coef coefficient de la matiere
	 * @throws MatiereDejaExistanteException
	 */
	public void ajouterMatiere(String m, double coef) throws MatiereDejaExistanteException{
		if (m instanceof String){
			if(!(this.ensembleMatiere.containsKey(m))){
				this.ensembleMatiere.put(m, coef);
			}else{
				throw new MatiereDejaExistanteException();
			}
		}
	}

	/**
	 * methode supprimant une matiere placee en parametre
	 * @param m matiere a supprimer
	 * @throws MatiereNonExistanteException
	 */
	public void supprimerMatiere(String m) throws MatiereNonExistanteException{
		if(this.ensembleMatiere.containsKey(m)){
			this.ensembleMatiere.remove(m);
		}else{
			throw new MatiereNonExistanteException();
		}
	}

	/**
	 * methode retournant le coefficient d'une matiere placee en parametre
	 * @param m matiere dont on veut connaitre le coefficient
	 * @return le coefficient de la matiere
	 * @throws MatiereNonExistanteException
	 */
	public double donnerCoefficient(String m) throws MatiereNonExistanteException{
		if(this.ensembleMatiere.containsKey(m)){
			return this.ensembleMatiere.get(m);
		}else{
			throw new MatiereNonExistanteException();
		}
	}
}
