package td2_cpoa_1;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FormationTest {

	Formation f;

	@Before
	public void init() {
		f = new Formation("Formation");
	}

	@Test
	public void TestAjMat() throws Exception {

		f.ajouterMatiere("Math", 9);
		f.ajouterMatiere("Histoire", 9);

		assertEquals("Il devrait y avoir 2 matières", 2, f.getEnsembleMatiere()
				.size());
	}
	
	@Test
	public void TestAjNull() throws Exception{
		f.ajouterMatiere(null, 1.0);
		assertTrue(f.getEnsembleMatiere().isEmpty());
	}
	
	@Test
	public void TestAjMatMatiereExc() throws Exception {

		f.ajouterMatiere("Math", 9);
		
		try{
		f.ajouterMatiere("Math", 4);
		}catch(MatiereDejaExistanteException e){	
		}
		f.ajouterMatiere("Histoire", 9);

		assertEquals("Il devrait y avoir 2 matières", 2, f.getEnsembleMatiere().size());
	}
	
	@Test(expected=MatiereDejaExistanteException.class)
	public void TestAjMatExc() throws Exception {

		f.ajouterMatiere("Math", 9);
		f.ajouterMatiere("Math", 4);
	
	}
	
	@Test
	public void TestSupMatExc() throws Exception {

		f.ajouterMatiere("A", 9);
		f.ajouterMatiere("B", 9);
		f.ajouterMatiere("C", 9);
		
		assertEquals("Il devrait y avoir 3 matières", 3, f.getEnsembleMatiere().size());
		
		f.supprimerMatiere("A");
		assertEquals("Il devrait y avoir 2 matières", 2, f.getEnsembleMatiere().size());
		f.supprimerMatiere("B");
		assertEquals("Il devrait y avoir 1 matières", 1, f.getEnsembleMatiere().size());
		f.supprimerMatiere("C");
		assertTrue("Doit être vide", f.getEnsembleMatiere().isEmpty());
	
	}
	
	@Test
	public void TestCoeff() throws Exception{
		f.ajouterMatiere("A", 3.0);
		assertTrue(3.0 == f.donnerCoefficient("A"));
	}
	
	@Test(expected = MatiereNonExistanteException.class)
	public void TestCoeffExc() throws Exception{
		assertTrue(3.0 == f.donnerCoefficient("A"));
	}

}
