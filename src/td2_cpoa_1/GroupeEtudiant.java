package td2_cpoa_1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class GroupeEtudiant {
	private ArrayList<Etudiant> ensembleEtudiants;
	private Formation formation;

	public GroupeEtudiant(Formation f){
		this.formation=f;
		this.ensembleEtudiants=new ArrayList<Etudiant>();
	}

	public ArrayList<Etudiant> getEnsembleEtudiants() {
		return ensembleEtudiants;
	}

	public Formation getFormation() {
		return formation;
	}

	public void ajouterEtudiant(Etudiant etudiant) throws etudiantNonConformeException{
		if(etudiant==null || this.ensembleEtudiants.indexOf(etudiant)!=-1 || !(etudiant.getFormation().getFormation().equals(this.formation.getFormation()))){
			throw new etudiantNonConformeException();
		}else{
			ensembleEtudiants.add(etudiant);
		}
	}

	public void supprimerEtudiant(Etudiant etudiant) throws etudiantNonConformeException{
		if(etudiant==null || this.ensembleEtudiants.indexOf(etudiant)==-1){
			throw new etudiantNonConformeException();
		}else{
			int i = this.ensembleEtudiants.indexOf(etudiant);
			this.ensembleEtudiants.remove(i);
		}
	}

	public double calculerMoyenneMatiereGroupe(String matiere) throws MatiereNonExistanteException{
		if(!this.formation.getEnsembleMatiere().containsKey(matiere)){
			throw new MatiereNonExistanteException();
		}else{
			double moyenne = 0;

			for (int i=0;i<this.ensembleEtudiants.size();i++){
				moyenne += this.ensembleEtudiants.get(i).calculerMoyenneMatiere(matiere);
			}

			return moyenne / this.ensembleEtudiants.size();
		}
	}

	public double calculerMoyenneGeneraleGroupe() throws MatiereNonExistanteException{
		Double res=0.0;
		if(this.ensembleEtudiants.size()>0){
			for(int i=0;i<this.ensembleEtudiants.size();i++){
				res += this.ensembleEtudiants.get(i).calculerMoyenneGeneral();
			}

			return res / this.ensembleEtudiants.size();
		}else{
			throw new NullPointerException();
		}
	}

	public void trierParMerite(){
		Collections.sort(this.ensembleEtudiants, new MeriteComparator());
	}
	public void trierAlpha(){
		Collections.sort(this.ensembleEtudiants, new Comparator<Etudiant>()
				{

					@Override
					public int compare(Etudiant o1, Etudiant o2) {
						String nomE1=o1.getIdentitite().getNom();
						String nomE2=o2.getIdentitite().getNom();
						String prenomE1=o1.getIdentitite().getPrenom();
						String prenomE2=o2.getIdentitite().getPrenom();
						if(nomE1.compareTo(nomE2)>0){
							return 1;
						}else if(nomE1.compareTo(nomE2) < 0){
							return -1;
						}else if(prenomE1.compareTo(prenomE2) > 0){
							return 1;
						}else if(prenomE1.compareTo(prenomE2) < 0){
							return -1;
						}else{
							return 0;
						}
					}
			
				});
	}
}
