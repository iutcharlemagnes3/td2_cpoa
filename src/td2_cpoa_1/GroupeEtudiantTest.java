package td2_cpoa_1;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class GroupeEtudiantTest {

	GroupeEtudiant gr;
	Etudiant e1, e2, e3;
	Formation a,b;
	
	@Before
	public void init() throws MatiereDejaExistanteException, IntervalleExcpetion, MatiereNonExistanteException {
		a = new Formation("A");
		b = new Formation("B");
		gr = new GroupeEtudiant(a);
		a.ajouterMatiere("Z", 2);
		b.ajouterMatiere("Y", 5.0);
		e1 = new Etudiant(a, new Identite("a", "a", "a"));
		e2 = new Etudiant(b, new Identite("b", "b", "b"));
		e3 = new Etudiant(a, new Identite("c", "c", "c"));
		e1.ajouterNote(10.0, "Z");
		e3.ajouterNote(11.0, "Z");
	}
	
	@Test
	public void AjEtudiantValidTest () throws etudiantNonConformeException{
		gr.ajouterEtudiant(e1);
		assertEquals("La liste doit contenir 1 etudiant", 1 ,gr.getEnsembleEtudiants().size());
	}
	
	@Test(expected = etudiantNonConformeException.class)
	public void AjEtudiantNonValidTest () throws etudiantNonConformeException{
		gr.ajouterEtudiant(e2);
		assertEquals("La liste doit contenir 0 etudiant", 0 ,gr.getEnsembleEtudiants().size());
	}
	
	@Test(expected = etudiantNonConformeException.class)
	public void AjEtudiantNullTest () throws etudiantNonConformeException{
		gr.ajouterEtudiant(null);
		assertEquals("La liste doit contenir 0 etudiant", 0 ,gr.getEnsembleEtudiants().size());
	}
	
	@Test
	public void SupEtudiantValidTest() throws etudiantNonConformeException{
		gr.ajouterEtudiant(e1);
		gr.supprimerEtudiant(e1);
		assertEquals("La liste doit contenir 0 etudiant", 0, gr.getEnsembleEtudiants().size());
	}
	
	@Test(expected = etudiantNonConformeException.class)
	public void SupEtudiantNullTest() throws etudiantNonConformeException{
		gr.ajouterEtudiant(e1);
		gr.supprimerEtudiant(null);
		assertEquals("La liste doit contenir 1 etudiant", 1, gr.getEnsembleEtudiants().size());
	}
	
	@Test(expected = etudiantNonConformeException.class)
	public void SupEtudiantNonTest() throws etudiantNonConformeException{
		gr.ajouterEtudiant(e1);
		gr.supprimerEtudiant(e2);
		assertEquals("La liste doit contenir 1 etudiant", 1, gr.getEnsembleEtudiants().size());
	}
	
	@Test
	public void CalculMatiereValidTest() throws etudiantNonConformeException, MatiereNonExistanteException{
		gr.ajouterEtudiant(e1);
		assertEquals("La moyenne doit être de 10", 10, gr.calculerMoyenneMatiereGroupe("Z"),0.01);
	}
	
	@Test(expected = MatiereNonExistanteException.class)
	public void CalculMatiereNonValidTest() throws etudiantNonConformeException, MatiereNonExistanteException{
		gr.ajouterEtudiant(e1);
		assertEquals(10, gr.calculerMoyenneMatiereGroupe("Y"),0.01);
	}	
	
	@Test(expected = MatiereNonExistanteException.class)
	public void CalculMatiereNullTest() throws etudiantNonConformeException, MatiereNonExistanteException{
		gr.ajouterEtudiant(e1);
		assertEquals(10, gr.calculerMoyenneMatiereGroupe(null),0.01);
	}
	
	@Test
	public void CalculGeneralValidTest() throws etudiantNonConformeException, MatiereNonExistanteException{
		gr.ajouterEtudiant(e1);
		assertEquals(10, gr.calculerMoyenneGeneraleGroupe(),0.01);
	}
	
	@Test(expected = NullPointerException.class)
	public void CalculGeneralSansEtuTest() throws etudiantNonConformeException, MatiereNonExistanteException, NullPointerException{
		System.out.println(gr.calculerMoyenneGeneraleGroupe());
	}
	
	@Test
	public void MeriteSortTest () throws etudiantNonConformeException{
		gr.ajouterEtudiant(e1);
		gr.ajouterEtudiant(e3);
		gr.trierParMerite();
		assertEquals("La premier doit être e3", e3, gr.getEnsembleEtudiants().get(0));
		assertEquals("La second doit être e1", e1, gr.getEnsembleEtudiants().get(1));
	}
	
	@Test
	public void AlphaSortTest () throws etudiantNonConformeException{
		gr.ajouterEtudiant(e3);
		gr.ajouterEtudiant(e1);
		gr.trierAlpha();
		assertEquals("La premier doit être e1", e1, gr.getEnsembleEtudiants().get(0));
		assertEquals("La second doit être e3", e3, gr.getEnsembleEtudiants().get(1));
	}
}
