package td2_cpoa_1;
/**
 * Classe modélisant une identite d'un étudiant
 * @author Capucine Blanchard, Maxime Gaudare
 */
public class Identite {

	String nip, nom, prenom;
	
	/**
	 * Construit une identité a partir de trois String représentant le numèro NIP, le nom et le prénom
	 * @param n, le numèro de l'étudiant
	 * @param no, le nom de l'étudiant
	 * @param pr, le prénom de l'étudiant
	 */
	public Identite (String pNip, String pNom, String pPrenom){
		this.nip = pNip;
		this.nom = pNom;
		this.prenom = pPrenom;
	}

	/**
	 * Renvoi le numèro NIP de l'étudiant
	 * @return un String représentant le NIP de l'étudiant
	 */
	public String getNip() {
		return this.nip;
	}

	/**
	 * Renvoi le nom de l'étudiant
	 * @return un String représentant le nom de l'étudiant
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Renvoi le prénom de l'étudiant
	 * @return un String représentant le prénom de l'étudiant
	 */
	public String getPrenom() {
		return this.prenom;
	}
}
