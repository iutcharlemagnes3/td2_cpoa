package td2_cpoa_1;

public class MatiereNonExistanteException extends Exception {

	public MatiereNonExistanteException(){
		super("La matière n'existe pas");
	}
}
