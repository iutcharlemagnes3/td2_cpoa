package td2_cpoa_1;
import java.util.Comparator;


public class MeriteComparator implements Comparator<Etudiant> {



	@Override
	public int compare(Etudiant etudiant1, Etudiant etudiant2) {
		double moyenne1=0.0;
		double moyenne2=0.0;
		try {
			moyenne1 = ((Etudiant) etudiant1).calculerMoyenneGeneral();
		} catch (MatiereNonExistanteException e) {
			System.out.println(e.getMessage());
		}
	
		try {
			moyenne2 = etudiant2.calculerMoyenneGeneral();
		} catch (MatiereNonExistanteException e) {
			System.out.println(e.getMessage());
		}
		if(moyenne1>moyenne2){
			return -1;
		}else if(moyenne1==moyenne2){
			return 0;
		}else{
			return 1;
		}
	}

}
